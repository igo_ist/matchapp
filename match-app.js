(function () {
  // создаем и возвращаем заголовок приложения
  function createAppTitle(title) {
    const appTitle = document.createElement('h2');
    appTitle.innerHTML = `Игра ${title}`;
    appTitle.classList.add('text-center')
    return appTitle;
  }

  // создаем и возвращаем форму для создания дела
  function createAppForm() {
    const form = document.createElement('form');
    const input = document.createElement('input');
    const buttonWrapper = document.createElement('div');
    const button = document.createElement('button');

    form.classList.add('input-group', 'mb-3');
    input.classList.add('form-control');
    input.placeholder = 'Кол-во карточек по вертикали/горизонтали';
    input.setAttribute('type', 'number');
    buttonWrapper.classList.add('input-group-append');
    button.classList.add('btn', 'btn-primary');
    button.textContent = 'Играть';

    buttonWrapper.append(button);
    form.append(input);
    form.append(buttonWrapper)

    return {
      form,
      input,
      button,
    };
  }

  // создаем и возвращаем список элементов
  function createGameContainer() {
    const div = document.createElement('div');
    div.classList.add('game-container');
    return div;
  }

  function createArrayCards(count) {
    const array = [];
    const cardsCount = count ** 2;

    for (let i = 1; i <= cardsCount / 2; i++) {
      array.push(i, i)
    }

    for (let i = array.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }

    return array;
  }

  function createCards(array, count) {
    const cardsContainer = document.createElement('div');
    const cardsCount = count ** 2;

    cardsContainer.classList.add('cards-group');

    function visible(element) {
      element.removeAttribute('hidded');
      element.setAttribute('visible', '')
      element.style.color = `#000`;
    };

    function hidded(element) {
      element.removeAttribute('visible');
      element.style.color = `transparent`;
      element.setAttribute('hidded', '');
    }

    function guessed(element) {
      element.removeAttribute('visible');
      element.setAttribute('guessed', '');
    }

    array.forEach((element, index) => {
      const card = document.createElement('button');
      const width = (100 / count) + '%';

      card.classList.add('btn', 'btn-card');
      card.style.width = `calc(${width} - 6px)`;
      hidded(card);
      card.textContent = element;

      card.addEventListener('click', function () {
        const list = cardsContainer.childNodes;

        if (card.hasAttribute('visible') || card.hasAttribute('guessed')) {
          return;
        }

        visible(card);

        let cardsVisible = document.querySelectorAll('button[visible]');

        if (cardsVisible.length >= 2) {
          list.forEach((elem, id) => {
            if ((elem.hasAttribute('visible') && card.hasAttribute('visible')) && (elem.textContent === card.textContent && (index !== id))) { // поиск совпадений
              console.log('Совпадение');
              guessed(elem);
              guessed(card);
            }
          });
          list.forEach((elem) => {
            if (!elem.hasAttribute('guessed') && elem.hasAttribute('visible')) {
              setTimeout(hidded, 500, elem);
            }
          });
        }
      });

      cardsContainer.append(card);
    });

    return cardsContainer;
  }

  function createTimer(count, time) {
    const timer = document.createElement('h3');
    const timerInterval = window.setInterval(decreaseSeconds, 1000);

    timer.classList.add('timer');

    let minutes = Math.floor(time / 60);
    let seconds = time % 60;

    timer.textContent = (minutes < 10 ? "0" : "") + String(minutes) + " : " + (seconds < 10 ? "0" : "") + String(seconds);

    function decreaseSeconds() {
      const currentTime = time;
      const cardsGuessed = document.querySelectorAll('.btn-card[guessed]');

      if (currentTime <= 1) {
        clearInterval(timerInterval);
        timer.textContent = 'Вы проиграли';
        timer.style.color = 'red';
        cardsDisabled();
        crateReloadButton(count);
      }
      else if (cardsGuessed.length === count ** 2) {
        clearInterval(timerInterval);
        timer.textContent = 'Вы выиграли';
        timer.style.color = 'green';
        cardsDisabled();
        crateReloadButton(count);
      }
      else {
        time--;
        minutes = Math.floor(time / 60);
        seconds = time % 60;
        timer.textContent = (minutes < 10 ? "0" : "") + String(minutes) + " : " + (seconds < 10 ? "0" : "") + String(seconds);
      }
    }

    function cardsDisabled() {
      const cardsContainer = document.querySelector('.cards-group');
      const list = cardsContainer.childNodes;
      list.forEach((elem) => {
        elem.setAttribute('disabled', '');
      });
    }
    return timer;
  }

  function createGamingField(count) {

    const arrayCards = createArrayCards(count);
    const cards = createCards(arrayCards, count);
    const time = count ** 3
    const timer = createTimer(count, time)

    const gameContainer = document.querySelector('.game-container')

    gameContainer.append(cards);
    gameContainer.prepend(timer);
  }


  function crateReloadButton(count) {
    const againButton = document.createElement('button');
    const gameContainer = document.querySelector('.game-container');

    againButton.classList.add('btn', 'btn-primary', 'btn-reload');
    againButton.textContent = 'Сыграть ещё раз';

    gameContainer.append(againButton);

    againButton.addEventListener('click', function () {

      while (gameContainer.firstChild) {
        gameContainer.firstChild.remove()
      }

      createGamingField(count);
    })
  }

  function createMatchApp(container, title) {

    const matchAppTitle = createAppTitle(title);
    const matchItemForm = createAppForm();
    const matchField = createGameContainer();

    container.append(matchAppTitle);
    container.append(matchItemForm.form);
    container.append(matchField);

    function checkInput() {
      !matchItemForm.input.value ?
        matchItemForm.button.setAttribute('disabled', '') :
        matchItemForm.button.removeAttribute('disabled');
    }
    checkInput();
    matchItemForm.input.addEventListener('input', checkInput);
    // браузер создает событие submit на форме по нажатию Enter или на кнопку создания дела
    matchItemForm.form.addEventListener('submit', function (e) {
      // эта строчка необходима, чтобы предотвратить стандартное действие браузера
      // в данном случае мы не хотим, чтобы страница перезагружалась при отправке формы
      e.preventDefault();

      function createGame(count) {
        matchItemForm.input.value = '';
        matchItemForm.form.remove();
        checkInput();
        createGamingField(count);
      }

      if (matchItemForm.input.valueAsNumber < 2 || matchItemForm.input.valueAsNumber > 10 || matchItemForm.input.valueAsNumber % 2 || !matchItemForm.input.value) {
        const result = confirm('Введите четное число\nВ диапазоне от 2 - 10\nИли продолжить с полем 4х4?')
        return result ? createGame(4) : null;
      }
      // создаем и добавляем в список новое дело с названием из поля для ввода
      const count = matchItemForm.input.valueAsNumber;
      // обнуляем значение в поле, чтобы не пришлось стирать его в ручную

      createGame(count);
    });
  }

  window.createMatchApp = createMatchApp;
})();
